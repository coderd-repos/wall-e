# 项目目录说明

| 目录 | 说明 |
| ---- | ---- |
| common | |
| components | 组件目录，存放项目自定义组件 |
| config | 配置目录，存放项目配置文件 |
| pages | 页面目录 |
| static | 静态资源目录 |
| store | vuex目录 |
| utils | 工具目录 |

# 底部导航配置

参考pages.json/tabBar属性配置。有以下地方需要注意：

1. 配置的页面必须在`pages.json/pages`中存在
2. 至少需要两个选项配置

# 异步接口调用

utils/request.js

示例：
```javascript
request.post('/goods/page', {
	page: 1,
	capacity: 1000
})
	.then(data => {
		this.goodsList = data.records
	})
	.catch(e => {
		uni.showToast({
			title: '数据加载失败',
			icon: null
		})
	})
```
