import Vue from 'vue'
import App from './App'
import Store from './store'
import filters from './filters'
import AppLayout from '@/layouts/AppLayout'
import WeIcon from '@/components/common/WeIcon'

Vue.config.productionTip = false
// 全局过滤器注册
Vue.use(filters)
// 全局组件注册
Vue.component('AppLayout', AppLayout)
Vue.component('WeIcon', WeIcon)

App.mpType = 'app'
App.store = Store

const app = new Vue({
  ...App
})
app.$mount()
