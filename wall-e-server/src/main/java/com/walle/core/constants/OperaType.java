package com.walle.core.constants;

/**
 * 操作类型
 * @author Wall-E.Caesar Liu
 * @date 2021/11/23 22:14
 */
public interface OperaType {

    /**
     * 创建
     * @author Wall-E.Caesar Liu
     * @date 2021/11/23 22:14
     */
    interface Create {}

    /**
     * 修改
     * @author Wall-E.Caesar Liu
     * @date 2021/11/23 22:14
     */
    interface Update {}

    /**
     * 修改状态
     * @author Wall-E.Caesar Liu
     * @date 2021/11/23 22:14
     */
    interface UpdateStatus {}
}
